import lenet
import data
import numpy as np
import matplotlib.pyplot as plt
import os
import torch

# declare some constants
model_dir = os.path.join(os.getcwd(), "new_models")

params = {'model': 'lenet',
          'display_step': 250, # we decreased this value for HW7
          'batch_size': 1024,
          'iterations': 30_000, # 5000 or 30000
          'initial_lr': 0.05, # we decreased this value for HW7
          'lr_decay': 0.5,
          'adjust_lr_step': 10_000,
          'initial_momentum': 0.5,
          'final_momentum': 0.95,
          'momentum_change_steps': 20_000,
          'adjust_momentum_step': 2_000,
          'apply_weight_norm': True,
          'weight_norm': 3.5,
          'adjust_norm_step': 5_000,
          'output_l2_decay': 0.001,
          'pooling': 'max', # TODO try three options: 'max', 'avg','no'
          'activation':'relu',
          'random_seed': 42}

def load_things(model_dir):
    os.chdir(model_dir)
    weights = [i for i in os.listdir() if i.split('_')[0] == 'weights']
    pools = [i.split('_')[-1].split('.')[0] for i in weights]
    modelz = {}
    trajs = {}
    for i in range(3):
        params['pooling'] = pools[i]
        clf = lenet.LeNet(params)
        clf.load_weights(weights[i])
        traj_file = f"trajectories_pool_{pools[i]}.pt"
        trajectories = torch.load(traj_file)
        trajs[pools[i]] = trajectories
        modelz[pools[i]] = clf

    return modelz, trajs

def eval_final(models, DATA):

    X_train, y_train, X_val, y_val, X_test, y_test = DATA.values()
    insample_dataloader = data.get_dataloader(X_train, y_train, batch_size=params['batch_size'])
    outsample_dataloader = data.get_dataloader(X_val, y_val, batch_size=params['batch_size'])

    mkeys = list(models.keys())
    train_losses = {}
    test_losses = {}
    train_acc = {}
    test_acc = {}

    for m in mkeys:
        model = models[m]

        train_losses[m] = model.evaluate_cross_entropy(insample_dataloader)
        test_losses[m] = model.evaluate_cross_entropy(outsample_dataloader)
        train_acc[m] = model.evaluate_accuracy(insample_dataloader)
        test_acc[m] = model.evaluate_accuracy(outsample_dataloader)

    return train_losses, test_losses, train_acc, test_acc


def part_b(train_acc, test_acc, trajectories):

    times = []
    train_ax = []
    test_ax = []
    for key in trajectories.keys():
        train_ax.append(train_acc[key])
        test_ax.append(test_acc[key])
        times.append(trajectories[key]['time'])



    fig, ax = plt.subplots(2, figsize=(6,8))

    w = 0.3
    offset = 0.3
    X = np.array([1, 2.5, 4])
    ax[0].bar(X - offset, train_ax, width=w, color='g', label='train', align='center')
    ax[0].bar(X + offset, test_ax, width=w, color='b', label='test', align='center')
    ax[1].bar(X, np.array(times) / 60., width=w*2)

    ax[0].set_title("Train, test accuracy for different pooling methods\nIterations = 30,000, batch_size = 1024")
    ax[0].set_xticks(X)
    ax[0].set_xticklabels(list(trajectories.keys()), rotation = 70)
    ax[0].set_ylabel("Accuracy")
    ax[0].set_ylim((80, 102))

    ax[1].set_title("Training time for different pooling methods\nIterations = 30,000, batch_size = 1024")
    ax[1].set_xticks(X)
    ax[1].set_xticklabels(list(trajectories.keys()), rotation = 70)
    ax[1].set_ylabel("Training time (minutes)")

    plt.tight_layout()

    return fig, ax


def part_cd(trajectories, y='loss'):

    fig, ax = plt.subplots(3, figsize=(8, 12), sharey=True)
    tkeys = list(trajectories.keys())

    if y == 'loss':
        train_key = 'insample_cross_entropy'
        test_key = 'outsample_cross_entropy'
        ylabel = "Average cross entropy"
        title = "Train and test loss using {0} pooling"
    else:
        train_key = 'insample_accuracy'
        test_key = 'outsample_accuracy'
        ylabel = "acuracy"
        title = "Train and test accuracy using {0} pooling"

    for i in range(3):
        model_name = tkeys[i]

        x_axis = np.arange(1, len(trajectories[tkeys[i]][train_key])+1)*250 # display step
        train_loss = np.array(trajectories[tkeys[i]][train_key])
        test_loss = np.array(trajectories[tkeys[i]][test_key])

        ax[i].plot(x_axis, train_loss, '.', linestyle='dashed', label='train')
        ax[i].plot(x_axis, test_loss, '.', linestyle='dashed', label='test')

        ax[i].set_xlabel("# Gradient steps")
        ax[i].set_ylabel("Average cross entropy")
        ax[i].set_title(title.format(tkeys[i]))

    plt.legend()
    plt.tight_layout()

    return fig, ax

def select_image(X_train, seed=None, idx=None):

    if idx:
        image = X_train[idx]
    else:
        if seed:
            np.random.seed(seed)
        random_int = np.random.randint(0, X_train.shape[0])
        image = X_train[random_int]

    return image

def gi_2(img, classifier):
    first_conv = classifier.model.input_layer[:2](img)
    mid_stuff = classifier.model.input_layer[2](first_conv)
    first_pool = classifier.model.input_layer[-1](mid_stuff)
    return first_conv.detach().numpy(), first_pool.detach().numpy()

def part_e(batch, classifier, seed=None, idx=None):
    """
    make the batch size 1

    1 28x28
    6 28x28
    6 14x14
    """
    img_first_conv, img_first_pool = gi_2(batch, classifier)
    fig = plt.figure(figsize = (12, 12))
    gs = fig.add_gridspec(12, 12)

    axes = []
    ax1 = fig.add_subplot(gs[:4,:2])
    axes.append(ax1)
    for i in range(6):
        mid_i = fig.add_subplot(gs[4:8,(i*2):(i+1)*2])
        axes.append(mid_i)

    for i in range(6):
        bot_i = fig.add_subplot(gs[8:, (i*2):(i+1)*2])
        axes.append(bot_i)

    axes[0].imshow(batch.reshape(28, 28))
    axes[0].set_title("Original Image")

    for i in range(6):
        axes[i+1].imshow(img_first_conv[0][i])
        axes[i+1].set_title(f"First conv\n{i+1}th filter")
        # if i == 0:
        #     axes[i+1].set_title("First conv\n1st filter")
        # else:
        #     axes[i+1].set_title(f"{i+1}th filter")

    for i in range(6):
        axes[i+1+6].imshow(img_first_pool[0][i])
        axes[i+1+6].set_title(f"First pool\n{i+1}th filter")
        # if i == 0:
        #     axes[i+1+6].set_title("First pool\n1st filter")
        # else:
        #     axes[i+1+6].set_title(f"{i+1}th filter")

    return fig


















    #
