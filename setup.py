from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

setup(name='LabelSmoothing',
      version='1.0',
      description='Experiments with label smoothing',
      author='Ben Cohen',
      author_email='benjapaulcohen@gmail.com',
      url='',
      package_dir = {'': '.'},
      packages=find_packages(where='.')
     )

