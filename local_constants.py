import os
import sys

# for all visual experiments
# vis_constants_dir = os.path.join("D: \Documents\AI\Visual_AI")
# if vis_constants_dir not in sys.path:
#     sys.path.append(vis_constants_dir)

# import vis_constants as vs

data_dir = "D:\Documents\AI\Visual_AI\Data"

MNIST_dir = os.path.join(data_dir, 'MNIST\processed')
CIFAR_dir = os.path.join(data_dir, 'cifar-10-batches-py')
HYMENOPTERA_dir = os.path.join(data_dir, 'hymenoptera_data')
PETS_dir = os.path.join(data_dir, 'oxford-iiit-pet')
